@file:Suppress("JAVA_MODULE_DOES_NOT_EXPORT_PACKAGE")

package com.bd.asn1.huawei

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import reactor.core.publisher.Flux
import ru.cwt.asn1.BytesToStringSerializer
import ru.cwt.asn1.huawei.huawei_msc.CallEventDataFile
import ru.cwt.asn1.huawei.huawei_msc.CallEventRecord
import ru.cwt.asn1.huawei.huawei_msc.HeaderRecord
import ru.cwt.asn1.huawei.huawei_msc.TrailerRecord
import sun.nio.ch.ChannelInputStream
import sun.nio.ch.FileChannelImpl
import java.io.InputStream

fun huaweiReaderWithoutNullPresentation(fis: InputStream): Flux<String>
    = Flux.just(*getHuwaweiOutputMsgJsonStrArrayWithoutNullPresentation(fis))

fun huaweiReaderWithNullPresentation(fis: InputStream): Flux<String>
    = Flux.just(*getHuwaweiOutputMsgJsonStrArrayWithNullPresentation(fis))


private fun getHuwaweiOutputMsgJsonStrArrayWithoutNullPresentation(fis: InputStream): Array<String> {
    var res = arrayOf<String>()
    fis.use {
        val callEventDataFile = CallEventDataFile()
        callEventDataFile.decode(it)
        val callEventRecords = callEventDataFile.callEventRecords
        val headerRecord = callEventDataFile.headerRecord
        headerRecord.sourceFileName = sourceFileNameViaReflection(fis)
        val trailerRecord = callEventDataFile.trailerRecord
        res = callEventRecords.callEventRecord.map { callEventRecord ->
            HuwaweiOutputMsg(headerRecord,callEventRecord, trailerRecord).toJsonStrWithoutNullPresentation()
        }.toTypedArray()
    }
    return res
}

private fun getHuwaweiOutputMsgJsonStrArrayWithNullPresentation(fis: InputStream): Array<String> {
    var res = arrayOf<String>()
    fis.use {
        val callEventDataFile = CallEventDataFile()
        callEventDataFile.decode(it)
        val callEventRecords = callEventDataFile.callEventRecords
        val headerRecord = callEventDataFile.headerRecord
        headerRecord.sourceFileName = sourceFileNameViaReflection(fis)
        val trailerRecord = callEventDataFile.trailerRecord
        res = callEventRecords.callEventRecord.map { callEventRecord ->
            HuwaweiOutputMsg(headerRecord,callEventRecord, trailerRecord).toJsonStrWithNullPresentation()
        }.toTypedArray()
    }
    return res
}

data class HuwaweiOutputMsg(
    val headerRecord: HeaderRecord,
    val callEventRecord: CallEventRecord,
    val trailerRecord: TrailerRecord

){
    fun toJsonStrWithoutNullPresentation(): String  {
        val om = ObjectMapper()
        om.setSerializationInclusion(JsonInclude.Include.NON_NULL)
        val module = SimpleModule()
        module.addSerializer(ByteArray::class.java, BytesToStringSerializer())
        om.registerModule(module)
        return om.writeValueAsString(this)
    }

    fun toJsonStrWithNullPresentation(): String  {
        val om = ObjectMapper()
        val module = SimpleModule()
        module.addSerializer(ByteArray::class.java, BytesToStringSerializer())
        om.registerModule(module)
        return om.writeValueAsString(this)
    }
}

private fun sourceFileNameViaReflection(fis: InputStream) : String {
    return if (fis is ChannelInputStream) {
        val sourceFileName = fis::class.java.getDeclaredField("ch").apply {
            isAccessible = true
        }.get(fis) as FileChannelImpl
        val filePath = sourceFileName::class.java.getDeclaredField("path").apply {
            isAccessible = true
        }.get(sourceFileName) as String
        filePath.split("/").last()
    }
    else{
         (fis::class.java.getDeclaredField("path").apply { isAccessible=true }.get(fis) as String)
             .split("/").last()
    }
}
