import com.bd.asn1.huawei.huaweiReaderWithNullPresentation
import com.bd.asn1.huawei.huaweiReaderWithoutNullPresentation
import java.io.File

fun main() {
    val fileName = "files/Sigma_b03281802.dat"
    val fis = File(fileName).inputStream()
    huaweiReaderWithoutNullPresentation(fis)
        .subscribe(
            {
                println("Message : $it ")
            },
            {
                println("error : $it")
            },
            {
                println("stream finished")
            }
        )

}
