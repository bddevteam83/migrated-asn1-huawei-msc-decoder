/**
 * This class file was automatically generated by jASN1 v1.10.0 (http://www.openmuc.org)
 */

package ru.cwt.asn1.huawei.huawei_msc;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.ReverseByteArrayOutputStream;
import org.openmuc.jasn1.ber.types.BerBoolean;
import org.openmuc.jasn1.ber.types.BerType;

import java.io.*;


public class CAMELInformation implements BerType, Serializable {

	private static final long serialVersionUID = 1L;

	public static final BerTag tag = new BerTag(BerTag.UNIVERSAL_CLASS, BerTag.CONSTRUCTED, 17);

	@JsonIgnore
	public byte[] code = null;
	private CAMELDestinationNumber cAMELDestinationNumber = null;
	private ConnectedNumber connectedNumber = null;
	private RoamingNumber roamingNumber = null;
	private ROUTE mscOutgoingROUTE = null;
	private TimeStamp seizureTime = null;
	private TimeStamp answerTime = null;
	private TimeStamp releaseTime = null;
	private CallDuration callDuration = null;
	private DataVolume dataVolume = null;
	private CAMELInitCFIndicator cAMELInitCFIndicator = null;
	private CauseForTerm causeForTerm = null;
	private ChangedParameters cAMELModification = null;
	private FreeFormatData freeFormatData = null;
	private Diagnostics diagnostics = null;
	private BerBoolean freeFormatDataAppend = null;
	private FreeFormatData freeFormatData2 = null;
	private BerBoolean freeFormatDataAppend2 = null;
	private TranslatedNumber translatedNumber = null;
	private AdditionalChgInfo additionalChgInfo = null;
	private DefaultCallHandling defaultCallHandling2 = null;
	private GsmSCFAddress gsmSCFAddress2 = null;
	private ServiceKey serviceKey2 = null;
	private CamelLegId legID = null;
	private PartialRecordType partialRecordType = null;
	
	public CAMELInformation() {
	}

	public CAMELInformation(byte[] code) {
		this.code = code;
	}

	public void setCAMELDestinationNumber(CAMELDestinationNumber cAMELDestinationNumber) {
		this.cAMELDestinationNumber = cAMELDestinationNumber;
	}

	public CAMELDestinationNumber getCAMELDestinationNumber() {
		return cAMELDestinationNumber;
	}

	public void setConnectedNumber(ConnectedNumber connectedNumber) {
		this.connectedNumber = connectedNumber;
	}

	public ConnectedNumber getConnectedNumber() {
		return connectedNumber;
	}

	public void setRoamingNumber(RoamingNumber roamingNumber) {
		this.roamingNumber = roamingNumber;
	}

	public RoamingNumber getRoamingNumber() {
		return roamingNumber;
	}

	public void setMscOutgoingROUTE(ROUTE mscOutgoingROUTE) {
		this.mscOutgoingROUTE = mscOutgoingROUTE;
	}

	public ROUTE getMscOutgoingROUTE() {
		return mscOutgoingROUTE;
	}

	public void setSeizureTime(TimeStamp seizureTime) {
		this.seizureTime = seizureTime;
	}

	public TimeStamp getSeizureTime() {
		return seizureTime;
	}

	public void setAnswerTime(TimeStamp answerTime) {
		this.answerTime = answerTime;
	}

	public TimeStamp getAnswerTime() {
		return answerTime;
	}

	public void setReleaseTime(TimeStamp releaseTime) {
		this.releaseTime = releaseTime;
	}

	public TimeStamp getReleaseTime() {
		return releaseTime;
	}

	public void setCallDuration(CallDuration callDuration) {
		this.callDuration = callDuration;
	}

	public CallDuration getCallDuration() {
		return callDuration;
	}

	public void setDataVolume(DataVolume dataVolume) {
		this.dataVolume = dataVolume;
	}

	public DataVolume getDataVolume() {
		return dataVolume;
	}

	public void setCAMELInitCFIndicator(CAMELInitCFIndicator cAMELInitCFIndicator) {
		this.cAMELInitCFIndicator = cAMELInitCFIndicator;
	}

	public CAMELInitCFIndicator getCAMELInitCFIndicator() {
		return cAMELInitCFIndicator;
	}

	public void setCauseForTerm(CauseForTerm causeForTerm) {
		this.causeForTerm = causeForTerm;
	}

	public CauseForTerm getCauseForTerm() {
		return causeForTerm;
	}

	public void setCAMELModification(ChangedParameters cAMELModification) {
		this.cAMELModification = cAMELModification;
	}

	public ChangedParameters getCAMELModification() {
		return cAMELModification;
	}

	public void setFreeFormatData(FreeFormatData freeFormatData) {
		this.freeFormatData = freeFormatData;
	}

	public FreeFormatData getFreeFormatData() {
		return freeFormatData;
	}

	public void setDiagnostics(Diagnostics diagnostics) {
		this.diagnostics = diagnostics;
	}

	public Diagnostics getDiagnostics() {
		return diagnostics;
	}

	public void setFreeFormatDataAppend(BerBoolean freeFormatDataAppend) {
		this.freeFormatDataAppend = freeFormatDataAppend;
	}

	public BerBoolean getFreeFormatDataAppend() {
		return freeFormatDataAppend;
	}

	public void setFreeFormatData2(FreeFormatData freeFormatData2) {
		this.freeFormatData2 = freeFormatData2;
	}

	public FreeFormatData getFreeFormatData2() {
		return freeFormatData2;
	}

	public void setFreeFormatDataAppend2(BerBoolean freeFormatDataAppend2) {
		this.freeFormatDataAppend2 = freeFormatDataAppend2;
	}

	public BerBoolean getFreeFormatDataAppend2() {
		return freeFormatDataAppend2;
	}

	public void setTranslatedNumber(TranslatedNumber translatedNumber) {
		this.translatedNumber = translatedNumber;
	}

	public TranslatedNumber getTranslatedNumber() {
		return translatedNumber;
	}

	public void setAdditionalChgInfo(AdditionalChgInfo additionalChgInfo) {
		this.additionalChgInfo = additionalChgInfo;
	}

	public AdditionalChgInfo getAdditionalChgInfo() {
		return additionalChgInfo;
	}

	public void setDefaultCallHandling2(DefaultCallHandling defaultCallHandling2) {
		this.defaultCallHandling2 = defaultCallHandling2;
	}

	public DefaultCallHandling getDefaultCallHandling2() {
		return defaultCallHandling2;
	}

	public void setGsmSCFAddress2(GsmSCFAddress gsmSCFAddress2) {
		this.gsmSCFAddress2 = gsmSCFAddress2;
	}

	public GsmSCFAddress getGsmSCFAddress2() {
		return gsmSCFAddress2;
	}

	public void setServiceKey2(ServiceKey serviceKey2) {
		this.serviceKey2 = serviceKey2;
	}

	public ServiceKey getServiceKey2() {
		return serviceKey2;
	}

	public void setLegID(CamelLegId legID) {
		this.legID = legID;
	}

	public CamelLegId getLegID() {
		return legID;
	}

	public void setPartialRecordType(PartialRecordType partialRecordType) {
		this.partialRecordType = partialRecordType;
	}

	public PartialRecordType getPartialRecordType() {
		return partialRecordType;
	}

	public int encode(OutputStream reverseOS) throws IOException {
		return encode(reverseOS, true);
	}

	public int encode(OutputStream reverseOS, boolean withTag) throws IOException {

		if (code != null) {
			for (int i = code.length - 1; i >= 0; i--) {
				reverseOS.write(code[i]);
			}
			if (withTag) {
				return tag.encode(reverseOS) + code.length;
			}
			return code.length;
		}

		int codeLength = 0;
		int sublength;

		if (partialRecordType != null) {
			codeLength += partialRecordType.encode(reverseOS, false);
			// write tag: CONTEXT_CLASS, PRIMITIVE, 24
			reverseOS.write(0x98);
			codeLength += 1;
		}
		
		if (legID != null) {
			sublength = legID.encode(reverseOS);
			codeLength += sublength;
			codeLength += BerLength.encodeLength(reverseOS, sublength);
			// write tag: CONTEXT_CLASS, CONSTRUCTED, 23
			reverseOS.write(0xB7);
			codeLength += 1;
		}
		
		if (serviceKey2 != null) {
			codeLength += serviceKey2.encode(reverseOS, false);
			// write tag: CONTEXT_CLASS, PRIMITIVE, 22
			reverseOS.write(0x96);
			codeLength += 1;
		}
		
		if (gsmSCFAddress2 != null) {
			codeLength += gsmSCFAddress2.encode(reverseOS, false);
			// write tag: CONTEXT_CLASS, PRIMITIVE, 21
			reverseOS.write(0x95);
			codeLength += 1;
		}
		
		if (defaultCallHandling2 != null) {
			codeLength += defaultCallHandling2.encode(reverseOS, false);
			// write tag: CONTEXT_CLASS, PRIMITIVE, 20
			reverseOS.write(0x94);
			codeLength += 1;
		}
		
		if (additionalChgInfo != null) {
			codeLength += additionalChgInfo.encode(reverseOS, false);
			// write tag: CONTEXT_CLASS, CONSTRUCTED, 19
			reverseOS.write(0xB3);
			codeLength += 1;
		}
		
		if (translatedNumber != null) {
			codeLength += translatedNumber.encode(reverseOS, false);
			// write tag: CONTEXT_CLASS, PRIMITIVE, 18
			reverseOS.write(0x92);
			codeLength += 1;
		}
		
		if (freeFormatDataAppend2 != null) {
			codeLength += freeFormatDataAppend2.encode(reverseOS, false);
			// write tag: CONTEXT_CLASS, PRIMITIVE, 17
			reverseOS.write(0x91);
			codeLength += 1;
		}
		
		if (freeFormatData2 != null) {
			codeLength += freeFormatData2.encode(reverseOS, false);
			// write tag: CONTEXT_CLASS, PRIMITIVE, 16
			reverseOS.write(0x90);
			codeLength += 1;
		}
		
		if (freeFormatDataAppend != null) {
			codeLength += freeFormatDataAppend.encode(reverseOS, false);
			// write tag: CONTEXT_CLASS, PRIMITIVE, 15
			reverseOS.write(0x8F);
			codeLength += 1;
		}
		
		if (diagnostics != null) {
			sublength = diagnostics.encode(reverseOS);
			codeLength += sublength;
			codeLength += BerLength.encodeLength(reverseOS, sublength);
			// write tag: CONTEXT_CLASS, CONSTRUCTED, 14
			reverseOS.write(0xAE);
			codeLength += 1;
		}
		
		if (freeFormatData != null) {
			codeLength += freeFormatData.encode(reverseOS, false);
			// write tag: CONTEXT_CLASS, PRIMITIVE, 13
			reverseOS.write(0x8D);
			codeLength += 1;
		}
		
		if (cAMELModification != null) {
			codeLength += cAMELModification.encode(reverseOS, false);
			// write tag: CONTEXT_CLASS, CONSTRUCTED, 12
			reverseOS.write(0xAC);
			codeLength += 1;
		}
		
		if (causeForTerm != null) {
			codeLength += causeForTerm.encode(reverseOS, false);
			// write tag: CONTEXT_CLASS, PRIMITIVE, 11
			reverseOS.write(0x8B);
			codeLength += 1;
		}
		
		if (cAMELInitCFIndicator != null) {
			codeLength += cAMELInitCFIndicator.encode(reverseOS, false);
			// write tag: CONTEXT_CLASS, PRIMITIVE, 10
			reverseOS.write(0x8A);
			codeLength += 1;
		}
		
		if (dataVolume != null) {
			codeLength += dataVolume.encode(reverseOS, false);
			// write tag: CONTEXT_CLASS, PRIMITIVE, 9
			reverseOS.write(0x89);
			codeLength += 1;
		}
		
		if (callDuration != null) {
			codeLength += callDuration.encode(reverseOS, false);
			// write tag: CONTEXT_CLASS, PRIMITIVE, 8
			reverseOS.write(0x88);
			codeLength += 1;
		}
		
		if (releaseTime != null) {
			codeLength += releaseTime.encode(reverseOS, false);
			// write tag: CONTEXT_CLASS, PRIMITIVE, 7
			reverseOS.write(0x87);
			codeLength += 1;
		}
		
		if (answerTime != null) {
			codeLength += answerTime.encode(reverseOS, false);
			// write tag: CONTEXT_CLASS, PRIMITIVE, 6
			reverseOS.write(0x86);
			codeLength += 1;
		}
		
		if (seizureTime != null) {
			codeLength += seizureTime.encode(reverseOS, false);
			// write tag: CONTEXT_CLASS, PRIMITIVE, 5
			reverseOS.write(0x85);
			codeLength += 1;
		}
		
		if (mscOutgoingROUTE != null) {
			sublength = mscOutgoingROUTE.encode(reverseOS);
			codeLength += sublength;
			codeLength += BerLength.encodeLength(reverseOS, sublength);
			// write tag: CONTEXT_CLASS, CONSTRUCTED, 4
			reverseOS.write(0xA4);
			codeLength += 1;
		}
		
		if (roamingNumber != null) {
			codeLength += roamingNumber.encode(reverseOS, false);
			// write tag: CONTEXT_CLASS, PRIMITIVE, 3
			reverseOS.write(0x83);
			codeLength += 1;
		}
		
		if (connectedNumber != null) {
			codeLength += connectedNumber.encode(reverseOS, false);
			// write tag: CONTEXT_CLASS, PRIMITIVE, 2
			reverseOS.write(0x82);
			codeLength += 1;
		}
		
		if (cAMELDestinationNumber != null) {
			codeLength += cAMELDestinationNumber.encode(reverseOS, false);
			// write tag: CONTEXT_CLASS, PRIMITIVE, 1
			reverseOS.write(0x81);
			codeLength += 1;
		}
		
		codeLength += BerLength.encodeLength(reverseOS, codeLength);

		if (withTag) {
			codeLength += tag.encode(reverseOS);
		}

		return codeLength;

	}

	public int decode(InputStream is) throws IOException {
		return decode(is, true);
	}

	public int decode(InputStream is, boolean withTag) throws IOException {
		int codeLength = 0;
		int subCodeLength = 0;
		BerTag berTag = new BerTag();

		if (withTag) {
			codeLength += tag.decodeAndCheck(is);
		}

		BerLength length = new BerLength();
		codeLength += length.decode(is);

		int totalLength = length.val;
		if (totalLength == -1) {
			subCodeLength += berTag.decode(is);

			if (berTag.tagNumber == 0 && berTag.tagClass == 0 && berTag.primitive == 0) {
				int nextByte = is.read();
				if (nextByte != 0) {
					if (nextByte == -1) {
						throw new EOFException("Unexpected end of input stream.");
					}
					throw new IOException("Decoded sequence has wrong end of contents octets");
				}
				codeLength += subCodeLength + 1;
				return codeLength;
			}
			if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 1)) {
				cAMELDestinationNumber = new CAMELDestinationNumber();
				subCodeLength += cAMELDestinationNumber.decode(is, false);
				subCodeLength += berTag.decode(is);
			}
			if (berTag.tagNumber == 0 && berTag.tagClass == 0 && berTag.primitive == 0) {
				int nextByte = is.read();
				if (nextByte != 0) {
					if (nextByte == -1) {
						throw new EOFException("Unexpected end of input stream.");
					}
					throw new IOException("Decoded sequence has wrong end of contents octets");
				}
				codeLength += subCodeLength + 1;
				return codeLength;
			}
			if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 2)) {
				connectedNumber = new ConnectedNumber();
				subCodeLength += connectedNumber.decode(is, false);
				subCodeLength += berTag.decode(is);
			}
			if (berTag.tagNumber == 0 && berTag.tagClass == 0 && berTag.primitive == 0) {
				int nextByte = is.read();
				if (nextByte != 0) {
					if (nextByte == -1) {
						throw new EOFException("Unexpected end of input stream.");
					}
					throw new IOException("Decoded sequence has wrong end of contents octets");
				}
				codeLength += subCodeLength + 1;
				return codeLength;
			}
			if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 3)) {
				roamingNumber = new RoamingNumber();
				subCodeLength += roamingNumber.decode(is, false);
				subCodeLength += berTag.decode(is);
			}
			if (berTag.tagNumber == 0 && berTag.tagClass == 0 && berTag.primitive == 0) {
				int nextByte = is.read();
				if (nextByte != 0) {
					if (nextByte == -1) {
						throw new EOFException("Unexpected end of input stream.");
					}
					throw new IOException("Decoded sequence has wrong end of contents octets");
				}
				codeLength += subCodeLength + 1;
				return codeLength;
			}
			if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.CONSTRUCTED, 4)) {
				subCodeLength += length.decode(is);
				mscOutgoingROUTE = new ROUTE();
				int choiceDecodeLength = mscOutgoingROUTE.decode(is, null);
				if (choiceDecodeLength != 0) {
					subCodeLength += choiceDecodeLength;
					subCodeLength += berTag.decode(is);
				}
				else {
					mscOutgoingROUTE = null;
				}

			}
			if (berTag.tagNumber == 0 && berTag.tagClass == 0 && berTag.primitive == 0) {
				int nextByte = is.read();
				if (nextByte != 0) {
					if (nextByte == -1) {
						throw new EOFException("Unexpected end of input stream.");
					}
					throw new IOException("Decoded sequence has wrong end of contents octets");
				}
				codeLength += subCodeLength + 1;
				return codeLength;
			}
			if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 5)) {
				seizureTime = new TimeStamp();
				subCodeLength += seizureTime.decode(is, false);
				subCodeLength += berTag.decode(is);
			}
			if (berTag.tagNumber == 0 && berTag.tagClass == 0 && berTag.primitive == 0) {
				int nextByte = is.read();
				if (nextByte != 0) {
					if (nextByte == -1) {
						throw new EOFException("Unexpected end of input stream.");
					}
					throw new IOException("Decoded sequence has wrong end of contents octets");
				}
				codeLength += subCodeLength + 1;
				return codeLength;
			}
			if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 6)) {
				answerTime = new TimeStamp();
				subCodeLength += answerTime.decode(is, false);
				subCodeLength += berTag.decode(is);
			}
			if (berTag.tagNumber == 0 && berTag.tagClass == 0 && berTag.primitive == 0) {
				int nextByte = is.read();
				if (nextByte != 0) {
					if (nextByte == -1) {
						throw new EOFException("Unexpected end of input stream.");
					}
					throw new IOException("Decoded sequence has wrong end of contents octets");
				}
				codeLength += subCodeLength + 1;
				return codeLength;
			}
			if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 7)) {
				releaseTime = new TimeStamp();
				subCodeLength += releaseTime.decode(is, false);
				subCodeLength += berTag.decode(is);
			}
			if (berTag.tagNumber == 0 && berTag.tagClass == 0 && berTag.primitive == 0) {
				int nextByte = is.read();
				if (nextByte != 0) {
					if (nextByte == -1) {
						throw new EOFException("Unexpected end of input stream.");
					}
					throw new IOException("Decoded sequence has wrong end of contents octets");
				}
				codeLength += subCodeLength + 1;
				return codeLength;
			}
			if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 8)) {
				callDuration = new CallDuration();
				subCodeLength += callDuration.decode(is, false);
				subCodeLength += berTag.decode(is);
			}
			if (berTag.tagNumber == 0 && berTag.tagClass == 0 && berTag.primitive == 0) {
				int nextByte = is.read();
				if (nextByte != 0) {
					if (nextByte == -1) {
						throw new EOFException("Unexpected end of input stream.");
					}
					throw new IOException("Decoded sequence has wrong end of contents octets");
				}
				codeLength += subCodeLength + 1;
				return codeLength;
			}
			if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 9)) {
				dataVolume = new DataVolume();
				subCodeLength += dataVolume.decode(is, false);
				subCodeLength += berTag.decode(is);
			}
			if (berTag.tagNumber == 0 && berTag.tagClass == 0 && berTag.primitive == 0) {
				int nextByte = is.read();
				if (nextByte != 0) {
					if (nextByte == -1) {
						throw new EOFException("Unexpected end of input stream.");
					}
					throw new IOException("Decoded sequence has wrong end of contents octets");
				}
				codeLength += subCodeLength + 1;
				return codeLength;
			}
			if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 10)) {
				cAMELInitCFIndicator = new CAMELInitCFIndicator();
				subCodeLength += cAMELInitCFIndicator.decode(is, false);
				subCodeLength += berTag.decode(is);
			}
			if (berTag.tagNumber == 0 && berTag.tagClass == 0 && berTag.primitive == 0) {
				int nextByte = is.read();
				if (nextByte != 0) {
					if (nextByte == -1) {
						throw new EOFException("Unexpected end of input stream.");
					}
					throw new IOException("Decoded sequence has wrong end of contents octets");
				}
				codeLength += subCodeLength + 1;
				return codeLength;
			}
			if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 11)) {
				causeForTerm = new CauseForTerm();
				subCodeLength += causeForTerm.decode(is, false);
				subCodeLength += berTag.decode(is);
			}
			if (berTag.tagNumber == 0 && berTag.tagClass == 0 && berTag.primitive == 0) {
				int nextByte = is.read();
				if (nextByte != 0) {
					if (nextByte == -1) {
						throw new EOFException("Unexpected end of input stream.");
					}
					throw new IOException("Decoded sequence has wrong end of contents octets");
				}
				codeLength += subCodeLength + 1;
				return codeLength;
			}
			if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.CONSTRUCTED, 12)) {
				cAMELModification = new ChangedParameters();
				subCodeLength += cAMELModification.decode(is, false);
				subCodeLength += berTag.decode(is);
			}
			if (berTag.tagNumber == 0 && berTag.tagClass == 0 && berTag.primitive == 0) {
				int nextByte = is.read();
				if (nextByte != 0) {
					if (nextByte == -1) {
						throw new EOFException("Unexpected end of input stream.");
					}
					throw new IOException("Decoded sequence has wrong end of contents octets");
				}
				codeLength += subCodeLength + 1;
				return codeLength;
			}
			if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 13)) {
				freeFormatData = new FreeFormatData();
				subCodeLength += freeFormatData.decode(is, false);
				subCodeLength += berTag.decode(is);
			}
			if (berTag.tagNumber == 0 && berTag.tagClass == 0 && berTag.primitive == 0) {
				int nextByte = is.read();
				if (nextByte != 0) {
					if (nextByte == -1) {
						throw new EOFException("Unexpected end of input stream.");
					}
					throw new IOException("Decoded sequence has wrong end of contents octets");
				}
				codeLength += subCodeLength + 1;
				return codeLength;
			}
			if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.CONSTRUCTED, 14)) {
				subCodeLength += length.decode(is);
				diagnostics = new Diagnostics();
				int choiceDecodeLength = diagnostics.decode(is, null);
				if (choiceDecodeLength != 0) {
					subCodeLength += choiceDecodeLength;
					subCodeLength += berTag.decode(is);
				}
				else {
					diagnostics = null;
				}

			}
			if (berTag.tagNumber == 0 && berTag.tagClass == 0 && berTag.primitive == 0) {
				int nextByte = is.read();
				if (nextByte != 0) {
					if (nextByte == -1) {
						throw new EOFException("Unexpected end of input stream.");
					}
					throw new IOException("Decoded sequence has wrong end of contents octets");
				}
				codeLength += subCodeLength + 1;
				return codeLength;
			}
			if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 15)) {
				freeFormatDataAppend = new BerBoolean();
				subCodeLength += freeFormatDataAppend.decode(is, false);
				subCodeLength += berTag.decode(is);
			}
			if (berTag.tagNumber == 0 && berTag.tagClass == 0 && berTag.primitive == 0) {
				int nextByte = is.read();
				if (nextByte != 0) {
					if (nextByte == -1) {
						throw new EOFException("Unexpected end of input stream.");
					}
					throw new IOException("Decoded sequence has wrong end of contents octets");
				}
				codeLength += subCodeLength + 1;
				return codeLength;
			}
			if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 16)) {
				freeFormatData2 = new FreeFormatData();
				subCodeLength += freeFormatData2.decode(is, false);
				subCodeLength += berTag.decode(is);
			}
			if (berTag.tagNumber == 0 && berTag.tagClass == 0 && berTag.primitive == 0) {
				int nextByte = is.read();
				if (nextByte != 0) {
					if (nextByte == -1) {
						throw new EOFException("Unexpected end of input stream.");
					}
					throw new IOException("Decoded sequence has wrong end of contents octets");
				}
				codeLength += subCodeLength + 1;
				return codeLength;
			}
			if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 17)) {
				freeFormatDataAppend2 = new BerBoolean();
				subCodeLength += freeFormatDataAppend2.decode(is, false);
				subCodeLength += berTag.decode(is);
			}
			if (berTag.tagNumber == 0 && berTag.tagClass == 0 && berTag.primitive == 0) {
				int nextByte = is.read();
				if (nextByte != 0) {
					if (nextByte == -1) {
						throw new EOFException("Unexpected end of input stream.");
					}
					throw new IOException("Decoded sequence has wrong end of contents octets");
				}
				codeLength += subCodeLength + 1;
				return codeLength;
			}
			if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 18)) {
				translatedNumber = new TranslatedNumber();
				subCodeLength += translatedNumber.decode(is, false);
				subCodeLength += berTag.decode(is);
			}
			if (berTag.tagNumber == 0 && berTag.tagClass == 0 && berTag.primitive == 0) {
				int nextByte = is.read();
				if (nextByte != 0) {
					if (nextByte == -1) {
						throw new EOFException("Unexpected end of input stream.");
					}
					throw new IOException("Decoded sequence has wrong end of contents octets");
				}
				codeLength += subCodeLength + 1;
				return codeLength;
			}
			if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.CONSTRUCTED, 19)) {
				additionalChgInfo = new AdditionalChgInfo();
				subCodeLength += additionalChgInfo.decode(is, false);
				subCodeLength += berTag.decode(is);
			}
			if (berTag.tagNumber == 0 && berTag.tagClass == 0 && berTag.primitive == 0) {
				int nextByte = is.read();
				if (nextByte != 0) {
					if (nextByte == -1) {
						throw new EOFException("Unexpected end of input stream.");
					}
					throw new IOException("Decoded sequence has wrong end of contents octets");
				}
				codeLength += subCodeLength + 1;
				return codeLength;
			}
			if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 20)) {
				defaultCallHandling2 = new DefaultCallHandling();
				subCodeLength += defaultCallHandling2.decode(is, false);
				subCodeLength += berTag.decode(is);
			}
			if (berTag.tagNumber == 0 && berTag.tagClass == 0 && berTag.primitive == 0) {
				int nextByte = is.read();
				if (nextByte != 0) {
					if (nextByte == -1) {
						throw new EOFException("Unexpected end of input stream.");
					}
					throw new IOException("Decoded sequence has wrong end of contents octets");
				}
				codeLength += subCodeLength + 1;
				return codeLength;
			}
			if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 21)) {
				gsmSCFAddress2 = new GsmSCFAddress();
				subCodeLength += gsmSCFAddress2.decode(is, false);
				subCodeLength += berTag.decode(is);
			}
			if (berTag.tagNumber == 0 && berTag.tagClass == 0 && berTag.primitive == 0) {
				int nextByte = is.read();
				if (nextByte != 0) {
					if (nextByte == -1) {
						throw new EOFException("Unexpected end of input stream.");
					}
					throw new IOException("Decoded sequence has wrong end of contents octets");
				}
				codeLength += subCodeLength + 1;
				return codeLength;
			}
			if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 22)) {
				serviceKey2 = new ServiceKey();
				subCodeLength += serviceKey2.decode(is, false);
				subCodeLength += berTag.decode(is);
			}
			if (berTag.tagNumber == 0 && berTag.tagClass == 0 && berTag.primitive == 0) {
				int nextByte = is.read();
				if (nextByte != 0) {
					if (nextByte == -1) {
						throw new EOFException("Unexpected end of input stream.");
					}
					throw new IOException("Decoded sequence has wrong end of contents octets");
				}
				codeLength += subCodeLength + 1;
				return codeLength;
			}
			if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.CONSTRUCTED, 23)) {
				subCodeLength += length.decode(is);
				legID = new CamelLegId();
				int choiceDecodeLength = legID.decode(is, null);
				if (choiceDecodeLength != 0) {
					subCodeLength += choiceDecodeLength;
					subCodeLength += berTag.decode(is);
				}
				else {
					legID = null;
				}

			}
			if (berTag.tagNumber == 0 && berTag.tagClass == 0 && berTag.primitive == 0) {
				int nextByte = is.read();
				if (nextByte != 0) {
					if (nextByte == -1) {
						throw new EOFException("Unexpected end of input stream.");
					}
					throw new IOException("Decoded sequence has wrong end of contents octets");
				}
				codeLength += subCodeLength + 1;
				return codeLength;
			}
			if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 24)) {
				partialRecordType = new PartialRecordType();
				subCodeLength += partialRecordType.decode(is, false);
				subCodeLength += berTag.decode(is);
			}
			int nextByte = is.read();
			if (berTag.tagNumber != 0 || berTag.tagClass != 0 || berTag.primitive != 0
			|| nextByte != 0) {
				if (nextByte == -1) {
					throw new EOFException("Unexpected end of input stream.");
				}
				throw new IOException("Decoded sequence has wrong end of contents octets");
			}
			codeLength += subCodeLength + 1;
			return codeLength;
		}

		while (subCodeLength < totalLength) {
			subCodeLength += berTag.decode(is);
			if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 1)) {
				cAMELDestinationNumber = new CAMELDestinationNumber();
				subCodeLength += cAMELDestinationNumber.decode(is, false);
			}
			else if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 2)) {
				connectedNumber = new ConnectedNumber();
				subCodeLength += connectedNumber.decode(is, false);
			}
			else if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 3)) {
				roamingNumber = new RoamingNumber();
				subCodeLength += roamingNumber.decode(is, false);
			}
			else if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.CONSTRUCTED, 4)) {
				subCodeLength += new BerLength().decode(is);
				mscOutgoingROUTE = new ROUTE();
				subCodeLength += mscOutgoingROUTE.decode(is, null);
			}
			else if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 5)) {
				seizureTime = new TimeStamp();
				subCodeLength += seizureTime.decode(is, false);
			}
			else if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 6)) {
				answerTime = new TimeStamp();
				subCodeLength += answerTime.decode(is, false);
			}
			else if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 7)) {
				releaseTime = new TimeStamp();
				subCodeLength += releaseTime.decode(is, false);
			}
			else if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 8)) {
				callDuration = new CallDuration();
				subCodeLength += callDuration.decode(is, false);
			}
			else if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 9)) {
				dataVolume = new DataVolume();
				subCodeLength += dataVolume.decode(is, false);
			}
			else if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 10)) {
				cAMELInitCFIndicator = new CAMELInitCFIndicator();
				subCodeLength += cAMELInitCFIndicator.decode(is, false);
			}
			else if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 11)) {
				causeForTerm = new CauseForTerm();
				subCodeLength += causeForTerm.decode(is, false);
			}
			else if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.CONSTRUCTED, 12)) {
				cAMELModification = new ChangedParameters();
				subCodeLength += cAMELModification.decode(is, false);
			}
			else if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 13)) {
				freeFormatData = new FreeFormatData();
				subCodeLength += freeFormatData.decode(is, false);
			}
			else if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.CONSTRUCTED, 14)) {
				subCodeLength += new BerLength().decode(is);
				diagnostics = new Diagnostics();
				subCodeLength += diagnostics.decode(is, null);
			}
			else if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 15)) {
				freeFormatDataAppend = new BerBoolean();
				subCodeLength += freeFormatDataAppend.decode(is, false);
			}
			else if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 16)) {
				freeFormatData2 = new FreeFormatData();
				subCodeLength += freeFormatData2.decode(is, false);
			}
			else if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 17)) {
				freeFormatDataAppend2 = new BerBoolean();
				subCodeLength += freeFormatDataAppend2.decode(is, false);
			}
			else if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 18)) {
				translatedNumber = new TranslatedNumber();
				subCodeLength += translatedNumber.decode(is, false);
			}
			else if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.CONSTRUCTED, 19)) {
				additionalChgInfo = new AdditionalChgInfo();
				subCodeLength += additionalChgInfo.decode(is, false);
			}
			else if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 20)) {
				defaultCallHandling2 = new DefaultCallHandling();
				subCodeLength += defaultCallHandling2.decode(is, false);
			}
			else if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 21)) {
				gsmSCFAddress2 = new GsmSCFAddress();
				subCodeLength += gsmSCFAddress2.decode(is, false);
			}
			else if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 22)) {
				serviceKey2 = new ServiceKey();
				subCodeLength += serviceKey2.decode(is, false);
			}
			else if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.CONSTRUCTED, 23)) {
				subCodeLength += new BerLength().decode(is);
				legID = new CamelLegId();
				subCodeLength += legID.decode(is, null);
			}
			else if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 24)) {
				partialRecordType = new PartialRecordType();
				subCodeLength += partialRecordType.decode(is, false);
			}
		}
		if (subCodeLength != totalLength) {
			throw new IOException("Length of set does not match length tag, length tag: " + totalLength + ", actual set length: " + subCodeLength);

		}
		codeLength += subCodeLength;

		return codeLength;
	}

	public void encodeAndSave(int encodingSizeGuess) throws IOException {
		ReverseByteArrayOutputStream reverseOS = new ReverseByteArrayOutputStream(encodingSizeGuess);
		encode(reverseOS, false);
		code = reverseOS.getArray();
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		appendAsString(sb, 0);
		return sb.toString();
	}

	public void appendAsString(StringBuilder sb, int indentLevel) {

		sb.append("{");
		boolean firstSelectedElement = true;
		if (cAMELDestinationNumber != null) {
			sb.append("\n");
			for (int i = 0; i < indentLevel + 1; i++) {
				sb.append("\t");
			}
			sb.append("cAMELDestinationNumber: ").append(cAMELDestinationNumber);
			firstSelectedElement = false;
		}
		
		if (connectedNumber != null) {
			if (!firstSelectedElement) {
				sb.append(",\n");
			}
			for (int i = 0; i < indentLevel + 1; i++) {
				sb.append("\t");
			}
			sb.append("connectedNumber: ").append(connectedNumber);
			firstSelectedElement = false;
		}
		
		if (roamingNumber != null) {
			if (!firstSelectedElement) {
				sb.append(",\n");
			}
			for (int i = 0; i < indentLevel + 1; i++) {
				sb.append("\t");
			}
			sb.append("roamingNumber: ").append(roamingNumber);
			firstSelectedElement = false;
		}
		
		if (mscOutgoingROUTE != null) {
			if (!firstSelectedElement) {
				sb.append(",\n");
			}
			for (int i = 0; i < indentLevel + 1; i++) {
				sb.append("\t");
			}
			sb.append("mscOutgoingROUTE: ");
			mscOutgoingROUTE.appendAsString(sb, indentLevel + 1);
			firstSelectedElement = false;
		}
		
		if (seizureTime != null) {
			if (!firstSelectedElement) {
				sb.append(",\n");
			}
			for (int i = 0; i < indentLevel + 1; i++) {
				sb.append("\t");
			}
			sb.append("seizureTime: ").append(seizureTime);
			firstSelectedElement = false;
		}
		
		if (answerTime != null) {
			if (!firstSelectedElement) {
				sb.append(",\n");
			}
			for (int i = 0; i < indentLevel + 1; i++) {
				sb.append("\t");
			}
			sb.append("answerTime: ").append(answerTime);
			firstSelectedElement = false;
		}
		
		if (releaseTime != null) {
			if (!firstSelectedElement) {
				sb.append(",\n");
			}
			for (int i = 0; i < indentLevel + 1; i++) {
				sb.append("\t");
			}
			sb.append("releaseTime: ").append(releaseTime);
			firstSelectedElement = false;
		}
		
		if (callDuration != null) {
			if (!firstSelectedElement) {
				sb.append(",\n");
			}
			for (int i = 0; i < indentLevel + 1; i++) {
				sb.append("\t");
			}
			sb.append("callDuration: ").append(callDuration);
			firstSelectedElement = false;
		}
		
		if (dataVolume != null) {
			if (!firstSelectedElement) {
				sb.append(",\n");
			}
			for (int i = 0; i < indentLevel + 1; i++) {
				sb.append("\t");
			}
			sb.append("dataVolume: ").append(dataVolume);
			firstSelectedElement = false;
		}
		
		if (cAMELInitCFIndicator != null) {
			if (!firstSelectedElement) {
				sb.append(",\n");
			}
			for (int i = 0; i < indentLevel + 1; i++) {
				sb.append("\t");
			}
			sb.append("cAMELInitCFIndicator: ").append(cAMELInitCFIndicator);
			firstSelectedElement = false;
		}
		
		if (causeForTerm != null) {
			if (!firstSelectedElement) {
				sb.append(",\n");
			}
			for (int i = 0; i < indentLevel + 1; i++) {
				sb.append("\t");
			}
			sb.append("causeForTerm: ").append(causeForTerm);
			firstSelectedElement = false;
		}
		
		if (cAMELModification != null) {
			if (!firstSelectedElement) {
				sb.append(",\n");
			}
			for (int i = 0; i < indentLevel + 1; i++) {
				sb.append("\t");
			}
			sb.append("cAMELModification: ");
			cAMELModification.appendAsString(sb, indentLevel + 1);
			firstSelectedElement = false;
		}
		
		if (freeFormatData != null) {
			if (!firstSelectedElement) {
				sb.append(",\n");
			}
			for (int i = 0; i < indentLevel + 1; i++) {
				sb.append("\t");
			}
			sb.append("freeFormatData: ").append(freeFormatData);
			firstSelectedElement = false;
		}
		
		if (diagnostics != null) {
			if (!firstSelectedElement) {
				sb.append(",\n");
			}
			for (int i = 0; i < indentLevel + 1; i++) {
				sb.append("\t");
			}
			sb.append("diagnostics: ");
			diagnostics.appendAsString(sb, indentLevel + 1);
			firstSelectedElement = false;
		}
		
		if (freeFormatDataAppend != null) {
			if (!firstSelectedElement) {
				sb.append(",\n");
			}
			for (int i = 0; i < indentLevel + 1; i++) {
				sb.append("\t");
			}
			sb.append("freeFormatDataAppend: ").append(freeFormatDataAppend);
			firstSelectedElement = false;
		}
		
		if (freeFormatData2 != null) {
			if (!firstSelectedElement) {
				sb.append(",\n");
			}
			for (int i = 0; i < indentLevel + 1; i++) {
				sb.append("\t");
			}
			sb.append("freeFormatData2: ").append(freeFormatData2);
			firstSelectedElement = false;
		}
		
		if (freeFormatDataAppend2 != null) {
			if (!firstSelectedElement) {
				sb.append(",\n");
			}
			for (int i = 0; i < indentLevel + 1; i++) {
				sb.append("\t");
			}
			sb.append("freeFormatDataAppend2: ").append(freeFormatDataAppend2);
			firstSelectedElement = false;
		}
		
		if (translatedNumber != null) {
			if (!firstSelectedElement) {
				sb.append(",\n");
			}
			for (int i = 0; i < indentLevel + 1; i++) {
				sb.append("\t");
			}
			sb.append("translatedNumber: ").append(translatedNumber);
			firstSelectedElement = false;
		}
		
		if (additionalChgInfo != null) {
			if (!firstSelectedElement) {
				sb.append(",\n");
			}
			for (int i = 0; i < indentLevel + 1; i++) {
				sb.append("\t");
			}
			sb.append("additionalChgInfo: ");
			additionalChgInfo.appendAsString(sb, indentLevel + 1);
			firstSelectedElement = false;
		}
		
		if (defaultCallHandling2 != null) {
			if (!firstSelectedElement) {
				sb.append(",\n");
			}
			for (int i = 0; i < indentLevel + 1; i++) {
				sb.append("\t");
			}
			sb.append("defaultCallHandling2: ").append(defaultCallHandling2);
			firstSelectedElement = false;
		}
		
		if (gsmSCFAddress2 != null) {
			if (!firstSelectedElement) {
				sb.append(",\n");
			}
			for (int i = 0; i < indentLevel + 1; i++) {
				sb.append("\t");
			}
			sb.append("gsmSCFAddress2: ").append(gsmSCFAddress2);
			firstSelectedElement = false;
		}
		
		if (serviceKey2 != null) {
			if (!firstSelectedElement) {
				sb.append(",\n");
			}
			for (int i = 0; i < indentLevel + 1; i++) {
				sb.append("\t");
			}
			sb.append("serviceKey2: ").append(serviceKey2);
			firstSelectedElement = false;
		}
		
		if (legID != null) {
			if (!firstSelectedElement) {
				sb.append(",\n");
			}
			for (int i = 0; i < indentLevel + 1; i++) {
				sb.append("\t");
			}
			sb.append("legID: ");
			legID.appendAsString(sb, indentLevel + 1);
			firstSelectedElement = false;
		}
		
		if (partialRecordType != null) {
			if (!firstSelectedElement) {
				sb.append(",\n");
			}
			for (int i = 0; i < indentLevel + 1; i++) {
				sb.append("\t");
			}
			sb.append("partialRecordType: ").append(partialRecordType);
			firstSelectedElement = false;
		}
		
		sb.append("\n");
		for (int i = 0; i < indentLevel; i++) {
			sb.append("\t");
		}
		sb.append("}");
	}

}

