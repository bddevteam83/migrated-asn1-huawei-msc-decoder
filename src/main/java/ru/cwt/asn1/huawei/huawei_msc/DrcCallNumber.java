/**
 * This class file was automatically generated by jASN1 v1.10.0 (http://www.openmuc.org)
 */

package ru.cwt.asn1.huawei.huawei_msc;

public class DrcCallNumber extends TBCDSTRING {

	private static final long serialVersionUID = 1L;

	public DrcCallNumber() {
	}

	public DrcCallNumber(byte[] value) {
		super(value);
	}

}
