/**
 * This class file was automatically generated by jASN1 v1.10.0 (http://www.openmuc.org)
 */

package ru.cwt.asn1.huawei.huawei_msc;

import org.openmuc.jasn1.ber.types.BerEnum;

import java.math.BigInteger;


public class SystemType extends BerEnum {

	private static final long serialVersionUID = 1L;

	public SystemType() {
	}

	public SystemType(byte[] code) {
		super(code);
	}

	public SystemType(BigInteger value) {
		super(value);
	}

	public SystemType(long value) {
		super(value);
	}

}
