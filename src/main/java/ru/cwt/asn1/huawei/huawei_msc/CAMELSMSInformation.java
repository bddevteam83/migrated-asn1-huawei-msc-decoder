/**
 * This class file was automatically generated by jASN1 v1.10.0 (http://www.openmuc.org)
 */

package ru.cwt.asn1.huawei.huawei_msc;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.openmuc.jasn1.ber.BerLength;
import org.openmuc.jasn1.ber.BerTag;
import org.openmuc.jasn1.ber.ReverseByteArrayOutputStream;
import org.openmuc.jasn1.ber.types.BerType;

import java.io.*;


public class CAMELSMSInformation implements BerType, Serializable {

	private static final long serialVersionUID = 1L;

	public static final BerTag tag = new BerTag(BerTag.UNIVERSAL_CLASS, BerTag.CONSTRUCTED, 17);

	@JsonIgnore
	public byte[] code = null;
	private GsmSCFAddress gsmSCFAddress = null;
	private ServiceKey serviceKey = null;
	private DefaultSMSHandling defaultSMSHandling = null;
	private FreeFormatData freeFormatData = null;
	private CallingNumber callingPartyNumber = null;
	private CalledNumber destinationSubscriberNumber = null;
	private AddressString cAMELSMSCAddress = null;
	private CallReferenceNumber smsReferenceNumber = null;
	
	public CAMELSMSInformation() {
	}

	public CAMELSMSInformation(byte[] code) {
		this.code = code;
	}

	public void setGsmSCFAddress(GsmSCFAddress gsmSCFAddress) {
		this.gsmSCFAddress = gsmSCFAddress;
	}

	public GsmSCFAddress getGsmSCFAddress() {
		return gsmSCFAddress;
	}

	public void setServiceKey(ServiceKey serviceKey) {
		this.serviceKey = serviceKey;
	}

	public ServiceKey getServiceKey() {
		return serviceKey;
	}

	public void setDefaultSMSHandling(DefaultSMSHandling defaultSMSHandling) {
		this.defaultSMSHandling = defaultSMSHandling;
	}

	public DefaultSMSHandling getDefaultSMSHandling() {
		return defaultSMSHandling;
	}

	public void setFreeFormatData(FreeFormatData freeFormatData) {
		this.freeFormatData = freeFormatData;
	}

	public FreeFormatData getFreeFormatData() {
		return freeFormatData;
	}

	public void setCallingPartyNumber(CallingNumber callingPartyNumber) {
		this.callingPartyNumber = callingPartyNumber;
	}

	public CallingNumber getCallingPartyNumber() {
		return callingPartyNumber;
	}

	public void setDestinationSubscriberNumber(CalledNumber destinationSubscriberNumber) {
		this.destinationSubscriberNumber = destinationSubscriberNumber;
	}

	public CalledNumber getDestinationSubscriberNumber() {
		return destinationSubscriberNumber;
	}

	public void setCAMELSMSCAddress(AddressString cAMELSMSCAddress) {
		this.cAMELSMSCAddress = cAMELSMSCAddress;
	}

	public AddressString getCAMELSMSCAddress() {
		return cAMELSMSCAddress;
	}

	public void setSmsReferenceNumber(CallReferenceNumber smsReferenceNumber) {
		this.smsReferenceNumber = smsReferenceNumber;
	}

	public CallReferenceNumber getSmsReferenceNumber() {
		return smsReferenceNumber;
	}

	public int encode(OutputStream reverseOS) throws IOException {
		return encode(reverseOS, true);
	}

	public int encode(OutputStream reverseOS, boolean withTag) throws IOException {

		if (code != null) {
			for (int i = code.length - 1; i >= 0; i--) {
				reverseOS.write(code[i]);
			}
			if (withTag) {
				return tag.encode(reverseOS) + code.length;
			}
			return code.length;
		}

		int codeLength = 0;
		if (smsReferenceNumber != null) {
			codeLength += smsReferenceNumber.encode(reverseOS, false);
			// write tag: CONTEXT_CLASS, PRIMITIVE, 8
			reverseOS.write(0x88);
			codeLength += 1;
		}
		
		if (cAMELSMSCAddress != null) {
			codeLength += cAMELSMSCAddress.encode(reverseOS, false);
			// write tag: CONTEXT_CLASS, PRIMITIVE, 7
			reverseOS.write(0x87);
			codeLength += 1;
		}
		
		if (destinationSubscriberNumber != null) {
			codeLength += destinationSubscriberNumber.encode(reverseOS, false);
			// write tag: CONTEXT_CLASS, PRIMITIVE, 6
			reverseOS.write(0x86);
			codeLength += 1;
		}
		
		if (callingPartyNumber != null) {
			codeLength += callingPartyNumber.encode(reverseOS, false);
			// write tag: CONTEXT_CLASS, PRIMITIVE, 5
			reverseOS.write(0x85);
			codeLength += 1;
		}
		
		if (freeFormatData != null) {
			codeLength += freeFormatData.encode(reverseOS, false);
			// write tag: CONTEXT_CLASS, PRIMITIVE, 4
			reverseOS.write(0x84);
			codeLength += 1;
		}
		
		if (defaultSMSHandling != null) {
			codeLength += defaultSMSHandling.encode(reverseOS, false);
			// write tag: CONTEXT_CLASS, PRIMITIVE, 3
			reverseOS.write(0x83);
			codeLength += 1;
		}
		
		if (serviceKey != null) {
			codeLength += serviceKey.encode(reverseOS, false);
			// write tag: CONTEXT_CLASS, PRIMITIVE, 2
			reverseOS.write(0x82);
			codeLength += 1;
		}
		
		if (gsmSCFAddress != null) {
			codeLength += gsmSCFAddress.encode(reverseOS, false);
			// write tag: CONTEXT_CLASS, PRIMITIVE, 1
			reverseOS.write(0x81);
			codeLength += 1;
		}
		
		codeLength += BerLength.encodeLength(reverseOS, codeLength);

		if (withTag) {
			codeLength += tag.encode(reverseOS);
		}

		return codeLength;

	}

	public int decode(InputStream is) throws IOException {
		return decode(is, true);
	}

	public int decode(InputStream is, boolean withTag) throws IOException {
		int codeLength = 0;
		int subCodeLength = 0;
		BerTag berTag = new BerTag();

		if (withTag) {
			codeLength += tag.decodeAndCheck(is);
		}

		BerLength length = new BerLength();
		codeLength += length.decode(is);

		int totalLength = length.val;
		if (totalLength == -1) {
			subCodeLength += berTag.decode(is);

			if (berTag.tagNumber == 0 && berTag.tagClass == 0 && berTag.primitive == 0) {
				int nextByte = is.read();
				if (nextByte != 0) {
					if (nextByte == -1) {
						throw new EOFException("Unexpected end of input stream.");
					}
					throw new IOException("Decoded sequence has wrong end of contents octets");
				}
				codeLength += subCodeLength + 1;
				return codeLength;
			}
			if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 1)) {
				gsmSCFAddress = new GsmSCFAddress();
				subCodeLength += gsmSCFAddress.decode(is, false);
				subCodeLength += berTag.decode(is);
			}
			if (berTag.tagNumber == 0 && berTag.tagClass == 0 && berTag.primitive == 0) {
				int nextByte = is.read();
				if (nextByte != 0) {
					if (nextByte == -1) {
						throw new EOFException("Unexpected end of input stream.");
					}
					throw new IOException("Decoded sequence has wrong end of contents octets");
				}
				codeLength += subCodeLength + 1;
				return codeLength;
			}
			if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 2)) {
				serviceKey = new ServiceKey();
				subCodeLength += serviceKey.decode(is, false);
				subCodeLength += berTag.decode(is);
			}
			if (berTag.tagNumber == 0 && berTag.tagClass == 0 && berTag.primitive == 0) {
				int nextByte = is.read();
				if (nextByte != 0) {
					if (nextByte == -1) {
						throw new EOFException("Unexpected end of input stream.");
					}
					throw new IOException("Decoded sequence has wrong end of contents octets");
				}
				codeLength += subCodeLength + 1;
				return codeLength;
			}
			if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 3)) {
				defaultSMSHandling = new DefaultSMSHandling();
				subCodeLength += defaultSMSHandling.decode(is, false);
				subCodeLength += berTag.decode(is);
			}
			if (berTag.tagNumber == 0 && berTag.tagClass == 0 && berTag.primitive == 0) {
				int nextByte = is.read();
				if (nextByte != 0) {
					if (nextByte == -1) {
						throw new EOFException("Unexpected end of input stream.");
					}
					throw new IOException("Decoded sequence has wrong end of contents octets");
				}
				codeLength += subCodeLength + 1;
				return codeLength;
			}
			if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 4)) {
				freeFormatData = new FreeFormatData();
				subCodeLength += freeFormatData.decode(is, false);
				subCodeLength += berTag.decode(is);
			}
			if (berTag.tagNumber == 0 && berTag.tagClass == 0 && berTag.primitive == 0) {
				int nextByte = is.read();
				if (nextByte != 0) {
					if (nextByte == -1) {
						throw new EOFException("Unexpected end of input stream.");
					}
					throw new IOException("Decoded sequence has wrong end of contents octets");
				}
				codeLength += subCodeLength + 1;
				return codeLength;
			}
			if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 5)) {
				callingPartyNumber = new CallingNumber();
				subCodeLength += callingPartyNumber.decode(is, false);
				subCodeLength += berTag.decode(is);
			}
			if (berTag.tagNumber == 0 && berTag.tagClass == 0 && berTag.primitive == 0) {
				int nextByte = is.read();
				if (nextByte != 0) {
					if (nextByte == -1) {
						throw new EOFException("Unexpected end of input stream.");
					}
					throw new IOException("Decoded sequence has wrong end of contents octets");
				}
				codeLength += subCodeLength + 1;
				return codeLength;
			}
			if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 6)) {
				destinationSubscriberNumber = new CalledNumber();
				subCodeLength += destinationSubscriberNumber.decode(is, false);
				subCodeLength += berTag.decode(is);
			}
			if (berTag.tagNumber == 0 && berTag.tagClass == 0 && berTag.primitive == 0) {
				int nextByte = is.read();
				if (nextByte != 0) {
					if (nextByte == -1) {
						throw new EOFException("Unexpected end of input stream.");
					}
					throw new IOException("Decoded sequence has wrong end of contents octets");
				}
				codeLength += subCodeLength + 1;
				return codeLength;
			}
			if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 7)) {
				cAMELSMSCAddress = new AddressString();
				subCodeLength += cAMELSMSCAddress.decode(is, false);
				subCodeLength += berTag.decode(is);
			}
			if (berTag.tagNumber == 0 && berTag.tagClass == 0 && berTag.primitive == 0) {
				int nextByte = is.read();
				if (nextByte != 0) {
					if (nextByte == -1) {
						throw new EOFException("Unexpected end of input stream.");
					}
					throw new IOException("Decoded sequence has wrong end of contents octets");
				}
				codeLength += subCodeLength + 1;
				return codeLength;
			}
			if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 8)) {
				smsReferenceNumber = new CallReferenceNumber();
				subCodeLength += smsReferenceNumber.decode(is, false);
				subCodeLength += berTag.decode(is);
			}
			int nextByte = is.read();
			if (berTag.tagNumber != 0 || berTag.tagClass != 0 || berTag.primitive != 0
			|| nextByte != 0) {
				if (nextByte == -1) {
					throw new EOFException("Unexpected end of input stream.");
				}
				throw new IOException("Decoded sequence has wrong end of contents octets");
			}
			codeLength += subCodeLength + 1;
			return codeLength;
		}

		while (subCodeLength < totalLength) {
			subCodeLength += berTag.decode(is);
			if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 1)) {
				gsmSCFAddress = new GsmSCFAddress();
				subCodeLength += gsmSCFAddress.decode(is, false);
			}
			else if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 2)) {
				serviceKey = new ServiceKey();
				subCodeLength += serviceKey.decode(is, false);
			}
			else if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 3)) {
				defaultSMSHandling = new DefaultSMSHandling();
				subCodeLength += defaultSMSHandling.decode(is, false);
			}
			else if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 4)) {
				freeFormatData = new FreeFormatData();
				subCodeLength += freeFormatData.decode(is, false);
			}
			else if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 5)) {
				callingPartyNumber = new CallingNumber();
				subCodeLength += callingPartyNumber.decode(is, false);
			}
			else if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 6)) {
				destinationSubscriberNumber = new CalledNumber();
				subCodeLength += destinationSubscriberNumber.decode(is, false);
			}
			else if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 7)) {
				cAMELSMSCAddress = new AddressString();
				subCodeLength += cAMELSMSCAddress.decode(is, false);
			}
			else if (berTag.equals(BerTag.CONTEXT_CLASS, BerTag.PRIMITIVE, 8)) {
				smsReferenceNumber = new CallReferenceNumber();
				subCodeLength += smsReferenceNumber.decode(is, false);
			}
		}
		if (subCodeLength != totalLength) {
			throw new IOException("Length of set does not match length tag, length tag: " + totalLength + ", actual set length: " + subCodeLength);

		}
		codeLength += subCodeLength;

		return codeLength;
	}

	public void encodeAndSave(int encodingSizeGuess) throws IOException {
		ReverseByteArrayOutputStream reverseOS = new ReverseByteArrayOutputStream(encodingSizeGuess);
		encode(reverseOS, false);
		code = reverseOS.getArray();
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		appendAsString(sb, 0);
		return sb.toString();
	}

	public void appendAsString(StringBuilder sb, int indentLevel) {

		sb.append("{");
		boolean firstSelectedElement = true;
		if (gsmSCFAddress != null) {
			sb.append("\n");
			for (int i = 0; i < indentLevel + 1; i++) {
				sb.append("\t");
			}
			sb.append("gsmSCFAddress: ").append(gsmSCFAddress);
			firstSelectedElement = false;
		}
		
		if (serviceKey != null) {
			if (!firstSelectedElement) {
				sb.append(",\n");
			}
			for (int i = 0; i < indentLevel + 1; i++) {
				sb.append("\t");
			}
			sb.append("serviceKey: ").append(serviceKey);
			firstSelectedElement = false;
		}
		
		if (defaultSMSHandling != null) {
			if (!firstSelectedElement) {
				sb.append(",\n");
			}
			for (int i = 0; i < indentLevel + 1; i++) {
				sb.append("\t");
			}
			sb.append("defaultSMSHandling: ").append(defaultSMSHandling);
			firstSelectedElement = false;
		}
		
		if (freeFormatData != null) {
			if (!firstSelectedElement) {
				sb.append(",\n");
			}
			for (int i = 0; i < indentLevel + 1; i++) {
				sb.append("\t");
			}
			sb.append("freeFormatData: ").append(freeFormatData);
			firstSelectedElement = false;
		}
		
		if (callingPartyNumber != null) {
			if (!firstSelectedElement) {
				sb.append(",\n");
			}
			for (int i = 0; i < indentLevel + 1; i++) {
				sb.append("\t");
			}
			sb.append("callingPartyNumber: ").append(callingPartyNumber);
			firstSelectedElement = false;
		}
		
		if (destinationSubscriberNumber != null) {
			if (!firstSelectedElement) {
				sb.append(",\n");
			}
			for (int i = 0; i < indentLevel + 1; i++) {
				sb.append("\t");
			}
			sb.append("destinationSubscriberNumber: ").append(destinationSubscriberNumber);
			firstSelectedElement = false;
		}
		
		if (cAMELSMSCAddress != null) {
			if (!firstSelectedElement) {
				sb.append(",\n");
			}
			for (int i = 0; i < indentLevel + 1; i++) {
				sb.append("\t");
			}
			sb.append("cAMELSMSCAddress: ").append(cAMELSMSCAddress);
			firstSelectedElement = false;
		}
		
		if (smsReferenceNumber != null) {
			if (!firstSelectedElement) {
				sb.append(",\n");
			}
			for (int i = 0; i < indentLevel + 1; i++) {
				sb.append("\t");
			}
			sb.append("smsReferenceNumber: ").append(smsReferenceNumber);
			firstSelectedElement = false;
		}
		
		sb.append("\n");
		for (int i = 0; i < indentLevel; i++) {
			sb.append("\t");
		}
		sb.append("}");
	}

}

