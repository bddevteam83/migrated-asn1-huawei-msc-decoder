/**
 * This class file was automatically generated by jASN1 v1.10.0 (http://www.openmuc.org)
 */

package ru.cwt.asn1.huawei.huawei_msc;

import org.openmuc.jasn1.ber.types.BerInteger;

import java.math.BigInteger;


public class MaximumNumberOfSMSInTheConcatenatedSMS extends BerInteger {

	private static final long serialVersionUID = 1L;

	public MaximumNumberOfSMSInTheConcatenatedSMS() {
	}

	public MaximumNumberOfSMSInTheConcatenatedSMS(byte[] code) {
		super(code);
	}

	public MaximumNumberOfSMSInTheConcatenatedSMS(BigInteger value) {
		super(value);
	}

	public MaximumNumberOfSMSInTheConcatenatedSMS(long value) {
		super(value);
	}

}
