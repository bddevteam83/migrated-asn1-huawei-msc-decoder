/**
 * This class file was automatically generated by jASN1 v1.10.0 (http://www.openmuc.org)
 */

package ru.cwt.asn1.huawei.huawei_msc;

import org.openmuc.jasn1.ber.types.BerOctetString;


public class ChargeAreaCode extends BerOctetString {

	private static final long serialVersionUID = 1L;

	public ChargeAreaCode() {
	}

	public ChargeAreaCode(byte[] value) {
		super(value);
	}

}
