/**
 * This class file was automatically generated by jASN1 v1.10.0 (http://www.openmuc.org)
 */

package ru.cwt.asn1.huawei.huawei_msc;

import org.openmuc.jasn1.ber.types.BerEnum;

import java.math.BigInteger;


public class NotificationToMSUser extends BerEnum {

	private static final long serialVersionUID = 1L;

	public NotificationToMSUser() {
	}

	public NotificationToMSUser(byte[] code) {
		super(code);
	}

	public NotificationToMSUser(BigInteger value) {
		super(value);
	}

	public NotificationToMSUser(long value) {
		super(value);
	}

}
