/**
 * This class file was automatically generated by jASN1 v1.10.0 (http://www.openmuc.org)
 */

package ru.cwt.asn1.huawei.huawei_msc;

public class SSActionResult extends Diagnostics {

	private static final long serialVersionUID = 1L;

	public SSActionResult() {
	}

	public SSActionResult(byte[] code) {
		super(code);
	}

}
