/**
 * This class file was automatically generated by jASN1 v1.10.0 (http://www.openmuc.org)
 */

package ru.cwt.asn1.huawei.huawei_msc;

import org.openmuc.jasn1.ber.types.BerBitString;


public class LevelOfCAMELService extends BerBitString {

	private static final long serialVersionUID = 1L;

	public LevelOfCAMELService() {
	}

	public LevelOfCAMELService(byte[] code) {
		super(code);
	}

	public LevelOfCAMELService(byte[] value, int numBits) {
		super(value, numBits);
	}

	public LevelOfCAMELService(boolean[] value) {
		super(value);
	}

}
