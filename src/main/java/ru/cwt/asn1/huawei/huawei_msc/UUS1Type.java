/**
 * This class file was automatically generated by jASN1 v1.10.0 (http://www.openmuc.org)
 */

package ru.cwt.asn1.huawei.huawei_msc;

import org.openmuc.jasn1.ber.types.BerEnum;

import java.math.BigInteger;


public class UUS1Type extends BerEnum {

	private static final long serialVersionUID = 1L;

	public UUS1Type() {
	}

	public UUS1Type(byte[] code) {
		super(code);
	}

	public UUS1Type(BigInteger value) {
		super(value);
	}

	public UUS1Type(long value) {
		super(value);
	}

}
