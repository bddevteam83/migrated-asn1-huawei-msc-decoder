/**
 * This class file was automatically generated by jASN1 v1.10.0 (http://www.openmuc.org)
 */

package ru.cwt.asn1.huawei.huawei_msc;

import org.openmuc.jasn1.ber.types.BerOctetString;


public class Classmark3 extends BerOctetString {

	private static final long serialVersionUID = 1L;

	public Classmark3() {
	}

	public Classmark3(byte[] value) {
		super(value);
	}

}
