plugins {
    `java-library`
    kotlin("jvm") version "1.3.40"
}

apply{
    plugin("java")
    plugin("maven")
}

java{
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}


group = "com.bd.parser"
version = "2.6-SNAPSHOT"

tasks.jar{
    enabled = true
}

repositories {

    mavenCentral()
    maven {
        isAllowInsecureProtocol = true
        url = uri("http://arti.bdinfra.net/repository/maven-central/")
        credentials {
            username = "app"
            password = "83energy18"
        }
    }

    maven {
        isAllowInsecureProtocol = true
        url = uri("http://arti.bdinfra.net/repository/maven-snapshots/")
        credentials {
            username = "app"
            password = "83energy18"
        }
    }
}


tasks.named<Upload>("uploadArchives") {
    repositories.withGroovyBuilder {
        "mavenDeployer" {
            "repository"("url" to uri("http://arti.bdinfra.net/repository/maven-releases/")) {
                "authentication"("userName" to "app", "password" to "83energy18")
            }
            "snapshotRepository"("url" to ("http://arti.bdinfra.net/repository/maven-snapshots/")) {
                "authentication"("userName" to "app", "password" to "83energy18")
            }
        }
    }
}


dependencies {
    implementation(kotlin("stdlib"))
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.6.0")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
    implementation(name = "jasn1", group = "org.openmuc", version = "1.10.0")
    implementation ("io.projectreactor:reactor-core:3.2.6.RELEASE")
    implementation (group = "com.fasterxml.jackson.core", name="jackson-databind", version = "2.12.2")
    implementation("commons-codec:commons-codec:1.15")
}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}

